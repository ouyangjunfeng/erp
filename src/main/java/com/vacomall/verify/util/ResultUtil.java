package com.vacomall.verify.util;

import com.vacomall.verify.ex.ProjException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root1 on 16/8/24.
 */
public class ResultUtil {

    private static final String RESULT = "result"; // 查询的结果
    private static final String KEY = "key";     //请求的路由
    private static final String QUERY = "query"; // 页面请求的查询参数
    private static final String CODE = "code";  // 放回的状态编码
    private static final String MSG = "msg";    //  内容

    public static Map<String, Object> wrapResult(String key, Object result, Object query) {
        Map<String, Object> rs = new HashMap<>();
        rs.put(RESULT, result);
        rs.put(QUERY, query);
        rs.put(KEY, key);
        rs.put(CODE, ResultCode.OK_CODE);
        return rs;
    }

    public static Map<String, Object> wrapErrorResult(String key, Object query, ProjException e) {
        Map<String, Object> rs = new HashMap<>();
        rs.put(QUERY, query);
        rs.put(KEY, key);
        rs.put(CODE, e.getCode());
        rs.put(MSG, e.getMsg());
        return rs;
    }

}

package com.vacomall.verify.util;

/**
 * Created by root1 on 16/8/24.
 */
public class ResultCode {

    public static final int ERROR_RESULT_DATA_CODE = 700;   //参数异常
    public static final int ERROR_PARAMS_DATA_CODE = 900;  //响应结果数据异常
    public static final int ERROR_PARAMS_NO_DATA_CODE = 901;  //响应结果数据库没有数据

    public static final int ERROR_DB_CODE = 800;  //Mysql数据库
    public static final int OK_CODE = 1000;


    public static final int ERROR_PARAMETER_DATA_CODE = 701 ; //参数不正确
    public static final int ERROR__NO_METHOD_CODE= 702;  //找不到指定的method
    public static final int ERROR_ILLEGAL_OPERATION_DATA_CODE = 703;  //参数异常,非法操作!

    public static final int ERROR_REDIS_INIT_CODE = 801 ; //Redis初始化异常
}

package com.vacomall.verify.dao;


import com.vacomall.verify.model.CertificateNum;

public interface CertificateNumMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CertificateNum record);

    int insertSelective(CertificateNum record);

    CertificateNum selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CertificateNum record);

    int updateByPrimaryKey(CertificateNum record);
    
    CertificateNum selectByNum(String number);
}
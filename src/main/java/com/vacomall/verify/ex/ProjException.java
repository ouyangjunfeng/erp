package com.vacomall.verify.ex;

import com.vacomall.verify.util.ResultCode;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by root1 on 16/8/24.
 */
public class ProjException extends RuntimeException  {
    private String msg;
    private int code;

    public ProjException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static ProjException get(Throwable e){
        return new ProjException(e);
    }

    public ProjException(Throwable e) {
        // 判断基本的类型异常,
        // 9xx 响应结果数据异常 IllegalFormatException
        // 8xx Mysql数据库
        // 7xx 请求参数校验异常 IllegalArgumentException
        // 6xx

        if (e instanceof InvocationTargetException) {
            if (((InvocationTargetException) e).getTargetException() instanceof IllegalArgumentException) {
                code =  ResultCode.ERROR_PARAMETER_DATA_CODE;
                msg = "参数不正确:" + ((InvocationTargetException) e).getTargetException().getMessage();
            } else if (((InvocationTargetException) e).getTargetException().getMessage().contains("SQL syntax")) {
                code = ResultCode.ERROR_ILLEGAL_OPERATION_DATA_CODE;
                msg = "参数异常,非法操作!";
            } else {
                e.printStackTrace();
            }
        } else if (e instanceof NoSuchMethodException) {
            code = ResultCode.ERROR__NO_METHOD_CODE;
            msg = "找不到指定的method:" + e.getMessage();
        } else if (e instanceof RuntimeException){
            code =  ResultCode.ERROR_PARAMETER_DATA_CODE;
            msg = "参数不正确:" + e.getMessage();
        } else {
            e.printStackTrace();
        }


    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}

package com.vacomall.verify.web;

import com.vacomall.verify.model.CertificateNum;
import com.vacomall.verify.service.ICertificateNumService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root1 on 16/8/23.
 */
@Controller
@RequestMapping(value = "upload")
public class UploadAction {

    final static Logger log = LoggerFactory.getLogger(UploadAction.class);

    @Autowired
    private ICertificateNumService certificateNumService;

    private final String IMAGES = "/Users/root1/IdeaProjects/vacomall_certificate/src/main/webapp/resource/imgs/";

    @RequestMapping(value="text",method = RequestMethod.GET)
    public String selectByNum(){
        return "uploadImg";
    }


    @RequestMapping(value = "imgs")
    public String upload(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request, ModelMap model) {
        List<String> list = new ArrayList<String>();
        String fileName = file.getOriginalFilename();
        File targetFile = new File(IMAGES, fileName);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        //保存
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        list.add(IMAGES + fileName);
        model.addAttribute("fileList", list);
        return "result";
    }



    /**
     * 图片下载显示
     * @param request
     * @param response
     * @param imageId
     *          图片的编号
     * @return
     */
    @RequestMapping(value = "/get")
    public ModelAndView get(HttpServletRequest request, HttpServletResponse response, String imageId) {
        log.debug("显示图片{}", imageId);
        FileInputStream fis = null;
        OutputStream outStream = null;
        try {
            outStream = response.getOutputStream();
            CertificateNum certificateNum = certificateNumService.selectByNum(imageId);
            if (certificateNum == null) {
                String message = "文件编号错误：" + imageId;
                outStream.write(message.getBytes());
                return null;
            }
            String image = certificateNum.getImgUrl();
            File file = new File(image);

            if (!file.exists()) {
                log.error("文件不存在：{}", image);
                String message = "文件不存在：" + image;
                outStream.write(message.getBytes());
                return null;
            }
          //  String type = getType(imageFile.getFromat());
          //  response.setContentType(type);
         //   response.setHeader("Content-Disposition", "attachment; filename=" + System.currentTimeMillis() + "." + certificateNum.getFromat());
            fis = new FileInputStream(image);
            byte[] b = new byte[1024];
            while (fis.read(b) != -1) {
                outStream.write(b);
            }
            outStream.flush();
        } catch (IOException e) {
            log.error("下载文件出现异常", e);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}

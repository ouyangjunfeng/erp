package com.vacomall.verify.web;

import com.vacomall.verify.ex.ProjException;
import com.vacomall.verify.model.CertificateNum;
import com.vacomall.verify.util.ResultCode;
import com.vacomall.verify.util.ResultUtil;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vacomall.verify.service.ICertificateNumService;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root1 on 16/8/23.
 */
@Controller
@RequestMapping(value = "certificateNum")
public class CertificateNumAction {
	
	@Autowired
	private ICertificateNumService certificateNumService;

	@RequestMapping(value="jianyi",method = RequestMethod.GET)
	public String jianyi() {
		return "jianyi";
	}
	
	
	@RequestMapping(value="selectByNum",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> selectByNum(@RequestParam("number") String number){
		CertificateNum certificateNum = certificateNumService.selectByNum(number);
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("number", number);
		if (certificateNum != null) {
			return ResultUtil.wrapResult("selectByNum", certificateNum, map);
		}else{
			return ResultUtil.wrapErrorResult("selectByNum", map,
					new ProjException(ResultCode.ERROR_PARAMS_NO_DATA_CODE, "无效检疫证书编号"));
		}


	}

    @RequestMapping(value = "updateCertificate" , method = RequestMethod.GET)
    @ResponseBody
    public String updateCertificate(CertificateNum certificateNum) {
        certificateNumService.updateCertifcate(certificateNum);
        return "ok";
    }
}

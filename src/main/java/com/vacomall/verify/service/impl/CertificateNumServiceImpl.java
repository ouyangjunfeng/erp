package com.vacomall.verify.service.impl;

import com.vacomall.verify.dao.CertificateNumMapper;
import com.vacomall.verify.model.CertificateNum;
import com.vacomall.verify.service.ICertificateNumService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by root1 on 16/8/23.
 */
@Service
public class CertificateNumServiceImpl implements ICertificateNumService {
	
	@Autowired
	private CertificateNumMapper certificateNumMapper;
	
	@Override
	public CertificateNum selectByNum(String number) {
		CertificateNum certificateNum = certificateNumMapper.selectByNum(number);
		return certificateNum;
	}

    @Override
    public int updateCertifcate(CertificateNum certificateNum) {
        int result = certificateNumMapper.updateByPrimaryKeySelective(certificateNum);
        return result;
    }
}

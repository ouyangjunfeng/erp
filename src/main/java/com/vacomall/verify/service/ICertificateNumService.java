package com.vacomall.verify.service;

import com.vacomall.verify.model.CertificateNum;

/**
 * Created by root1 on 16/8/23.
 */
public interface ICertificateNumService {
	
	public CertificateNum selectByNum(String number);

    public int updateCertifcate(CertificateNum certificateNum);

}

// JavaScript Document

var path="http://localhost:8080";
var img_show="";
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
$(function(){
	function fixPagesHeight() {
		var swidth = document.body.clientWidth
		var nsize = swidth/7.2
		if(parseInt(nsize)<150){
			$("html").css("font-size",nsize)
		}else{
			$("html").css("font-size","150px")
		}
	};
	$(window).resize(function(){
		fixPagesHeight()
	});
	fixPagesHeight()
	$(".btn_jianyi").click(function(){
		var jianyi_num = $(".txt_jianyi").val();
		if(jianyi_num==null){
			return;
		}
		if(jianyi_num==""){
			$(".app_jianyi_no").show();
			return;
		}
		console.log(jianyi_num);
		$.ajax({
			url:path+"/certificateNum/selectByNum" ,
			type:"post",
			data:{"number":jianyi_num},
			datatype:"json",
			success: function(whe){
				if(whe.code==1000){
					$(".app_jianyi_run").show();
					$(".app_jianyi_pic").show(200);
					img_show ="../upload/get?imageId="+whe.result.number;
					$(".app_jianyi_img img").attr("src","../upload/get?imageId="+whe.result.number);

					$("#jianyi_certificate_num").text("检疫证书编号："+whe.result.number);
					$("#jianyi_certificate_time").text("检疫时间："+whe.result.createTime);
					$(".app_jianyi_pic").show;
				}
				if(whe.code==901){
					$(".app_jianyi_pic").hide();
					$(".app_jianyi_run").hide();
					$(".app_jianyi_no").show();
				}
			},
			error: function(){
				alert("系统异常");

			},
		});

	})
	$(".app_cover").mouseenter(function(){
		$(this).hide()
	})
	$(".tip_close").click(function(){
		$(".app_jianyi_pic").hide()
		$(".app_jianyi_run").hide()
	})
	$(".app_jianyi_img img").click(function(){
		if(img_show==""){
			alert("未找到图片");
		}
		$(".app_jianyi_imgshow img").attr("src",img_show);
		$(".app_jianyi_imgshow").show()
	})
	$(".tip_close2").click(function(){
		$(".app_jianyi_no").hide();
		$(".app_jianyi_imgshow").hide()
	})
	$(".text_del").click(function(){
		$(".app_jianyi_no").hide();
		$(".txt_jianyi").val("");
	})
})